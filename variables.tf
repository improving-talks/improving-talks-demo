variable "environment" {
  description = "The name of the environment being deployed to"
  type        = string
}

variable "vpc_cidr" {
  type        = string
  description = "CIDR range for the VPC in this environment"
}

variable "availability_zones" {
  type = list(string)
}

