locals {
  subnets          = cidrsubnets(var.vpc_cidr, 3, 3, 3, 3, 3, 3, 4, 4, 4)
  private_subnets  = [local.subnets[0], local.subnets[1], local.subnets[2]]
  public_subnets   = [local.subnets[3], local.subnets[4], local.subnets[5]]
  database_subnets = [local.subnets[6], local.subnets[7], local.subnets[8]]

  tags = {
    environment = var.environment
  }
}