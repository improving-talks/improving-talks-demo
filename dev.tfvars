environment        = "dev"
vpc_cidr           = "10.10.0.0/23"
availability_zones = ["us-east-1a", "us-east-1b", "us-east-1c"]
