module "vpc" {
  source = "git::https://gitlab.com/improving-talks/central_source/vpc.git?ref=v0.1.0"

  name             = "talk-demo"
  environment      = var.environment
  cidr             = var.vpc_cidr
  azs              = var.availability_zones
  private_subnets  = local.private_subnets
  public_subnets   = local.public_subnets
  database_subnets = local.database_subnets
  tags             = local.tags
}

module "jumpbox" {
  source = "git::https://gitlab.com/improving-talks/central_source/jumpbox.git?ref=v0.1.0"

  vpc_id    = module.vpc.id
  subnet_id = module.vpc.public_subnet_ids[0]
}